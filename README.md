# Hypernode Vagrant image #
 
The Hypernode Vagrant images is a fork of from the Byte Hypernode Vagrant image. We have made some modifications to the basic configuration which suits our needs. 
 
# Installation #

Make sure you have the Virtualbox and Vagrant software installed. Install some Vagrant plugins with the following commands: "vagrant plugin install vagrant-hostmanager" and "vagrant plugin install vagrant-vbguest".

Checkout or download this repository and place the files in some directory. Execute the command "vagrant up" in order to het the virtual machine up and running and start developing. 
 
# Usage #
 
You can place the files in the root where the Vagrant files are also located. Magento 2 projects can be developed as a default. A local.yml example for Magento 1 projects is also provided. 
 
# Contribute #
 
To help push the 'Hypernode Vagrant image' forward please fork the Bitbucket repository and submit a pull request with your changes.
 
# License #
 
This module is distributed under the following licenses:
 
* Academic Free License ("AFL") v. 3.0
* Open Software License v. 3.0 (OSL-3.0)
 
# Authors #

* [Byte](https://github.com/ByteInternet/hypernode-vagrant/) 
* [Cream](https://www.cream.nl/)
 
# About Cream #
 
Cream is an ecommerce solution provider for the Magento platform. You'll find an overview of all our (open source) projects [on our website](https://www.cream.nl/).